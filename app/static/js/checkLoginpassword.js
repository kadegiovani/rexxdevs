function loginSuccess() {
  toastr.success("Acessando sua página inicial")
  window.location.href = "/"
}

$("#login").submit(function () {
  $("#button").prop("disabled", true)
  $("#img").show()
  event.preventDefault()
  $.ajax({
    url: "/checkloginpassword",
    data: $(this).serialize(),
    type: "POST",
    success: function (response) {
      $("#img").hide()
      if (response === "correct") {
        setTimeout(loginSuccess, 5000)
        swal
          .fire({
            icon: "success",
            title: "Bem Vindo",
          })
          .then((result) => {
            window.location.href = "/"
          })
      } else if (response === "wrong") {
        swal
          .fire({
            icon: "error",
            title: "Login Error",
            text: "Dados incorretos, verifique!",
          })
          .then((result) => {
            $("#button").removeAttr("disabled")
          })
      }
    },
    error: function (error) {
      console.log(error)
    },
  })
})
